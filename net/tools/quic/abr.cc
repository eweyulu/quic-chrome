#include "net/tools/quic/abr.h"
#include "net/tools/quic/bola.h"
#include "net/tools/quic/tput.h"
#include "net/tools/quic/mpc.h"

#define SLST_DEBUG 1

double Dispatcher::GetQuality(BolaAbr &bola) {
	double pause = 0.0;

	double tp = transport_->GetThroughput();
	uint32_t time = transport_->GetTime();
  bool was_in_time = transport_->GetInTime();

	#ifdef SLST_DEBUG
	std::cerr << "throughput: " << tp << " time: " << time << " late?: " << !was_in_time << std::endl;
	#endif

	int q = bola.GetQuality(tp, &pause);
	bola.PostUpdate(pause, time);

	//return (pause > 0.0)? -(pause) : (double)q;
	return (double)q;
}

double Dispatcher::GetQuality(ThroughputAbr &tput) {
  double pause = 0.0;

  double tp = transport_->GetThroughput();
  uint32_t time = transport_->GetTime();
  bool was_in_time = transport_->GetInTime();

#ifdef SLST_DEBUG
  std::cerr << "throughput: " << tp << " time: " << time << " late?: "
            << !was_in_time << std::endl;
#endif

  int q = tput.GetQuality(tp, &pause);
  tput.PostUpdate(pause, time);

  return (double)q;
}

double Dispatcher::GetQuality(MpcAbr &mpc) {
  double pause = 0.0;

  double tp = transport_->GetThroughput();
  uint32_t time = transport_->GetTime();
  bool was_in_time = transport_->GetInTime();
  double segment_size = transport_->GetSegmentSize();

#ifdef SLST_DEBUG
  std::cerr << "throughput: " << tp << " time: " << time << " late?: "
            << !was_in_time << std::endl;
#endif

  int q = mpc.GetQuality(tp, &pause);
  mpc.PostUpdate(pause, time, segment_size);

  return (double)q;
}


MovingAverage::MovingAverage()
    : throughput_slow_(0.0),
      throughput_fast_(0.0),
      cumulative_time_(0.0)
{
}

void MovingAverage::AddMeasurement(double throughput, double time)
{
  double alpha = std::pow(0.5, time / kHalfLifeSlow);
  throughput_slow_ = alpha * throughput_slow_ + (1.0 - alpha) * throughput;
  alpha = std::pow(0.5, time / kHalfLifeFast);
  throughput_fast_ = alpha * throughput_fast_ + (1.0 - alpha) * throughput;
  cumulative_time_ += time;
}

double MovingAverage::GetThroughput()
{
  if (cumulative_time_ <= 0.0) {
    return 0.0;
  }
  // zero factor avoids low estimates until average warms up
  double zero_factor = 1.0 - std::pow(0.5, cumulative_time_ / kHalfLifeSlow);
  double slow = throughput_slow_ / zero_factor;
  zero_factor = 1.0 - std::pow(0.5, cumulative_time_ / kHalfLifeFast);
  double fast = throughput_fast_ / zero_factor;
  return std::min(slow, fast);
}
