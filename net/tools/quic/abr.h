#ifndef ABR_SCHEMES
#define ABR_SCHEMES

#include "net/tools/quic/quic_simple_client.h"

class TransportInterface {
	public:
		virtual double GetThroughput() = 0;
		virtual uint32_t GetTime() = 0;
		virtual bool GetInTime() = 0;
		virtual double GetSegmentSize() = 0;
};

class BolaAbr;
class ThroughputAbr;
class MpcAbr;

class AbstractDispatcher {
	public:
		virtual double GetQuality(BolaAbr &bola) = 0;
		virtual double GetQuality(ThroughputAbr &tput) = 0;
		virtual double GetQuality(MpcAbr &mpc) = 0;
};

class BaseAbr {
public:
	virtual double accept(AbstractDispatcher &dispatcher) = 0;
};

struct AbrLogLine {
  double playhead_time_;
  double buffer_level_;
  double throughput_;
  int quality_;
  double bitrate_;
  double pause_;
};

enum ThroughputEstimates {
  kTPcoarse = 0,
  kTPfine,
  kTPjslike,
  kTPmoving
};

class MovingAverage {
 public:
  MovingAverage();
  void AddMeasurement(double bandwidth, double time);
  double GetThroughput();
 private:
  double throughput_slow_;
  double throughput_fast_;
  double cumulative_time_;
  static constexpr double kHalfLifeSlow = 8000;
  static constexpr double kHalfLifeFast = 3000;
};



class Dispatcher : public AbstractDispatcher{
	public:
		Dispatcher(TransportInterface* transport) 
		: transport_(transport) {};

		double GetQuality(BolaAbr &bola) override;
		double GetQuality(ThroughputAbr &tput) override;
		double GetQuality(MpcAbr &mpc) override;

	private:
		TransportInterface* transport_;
};

class Abr {
	public:
		Abr(BaseAbr* abr, TransportInterface* transport) 
		: disp(Dispatcher(transport)),
		  abr_(abr) {};

	double GetQuality() {
		return abr_->accept(disp);
	};

	BaseAbr* instance() {
		return abr_;
	}

private:
	Dispatcher disp;
	BaseAbr* abr_;
};

#endif //ABR_SCHEMES
