#include "net/tools/quic/abr.h"
#include "net/tools/quic/bola.h"

const bool kVerbose = true;

BolaAbr::~BolaAbr() {

}

double BolaAbr::accept(AbstractDispatcher &dispatcher) {
    return dispatcher.GetQuality(*this);
}

BolaAbr::BolaAbr(BolaVariant variant,
                 double segment_duration,
                 double buffer_size,
                 std::vector<double> bitrates)
    : buffer_level_(0.0),
      last_quality_(-1),
      placeholder_(0.0),
      variant_(variant),
      segment_duration_(segment_duration),
      buffer_size_(buffer_size),
      bitrates_(bitrates),
      utilities_(bitrates.size())
{
  for (size_t i = 0; i < bitrates_.size(); ++i) {
    utilities_[i] = std::log(bitrates_[i] / bitrates_[0]);
  }

  double alpha = (bitrates_[0] * utilities_[1] - bitrates_[1] * utilities_[0])
      / (bitrates_[1] - bitrates_[0]);
  double buffer_target = buffer_size_ - segment_duration_;
  if (variant == kBolaE) {
    // We might need buffer expansion technique
    double minimum_target = kBufferLow + kMinThreshold * bitrates_.size();
    if (buffer_target < minimum_target) {
      buffer_target = minimum_target;
    }
  }
  vp_ = (buffer_target - kBufferLow) / (utilities_.back() + alpha);
  gp_ = (utilities_.back() * kBufferLow + alpha * buffer_target)
      / (buffer_target - kBufferLow);

  if (kVerbose) {
    std::cerr << "BOLA:" << std::endl;
    std::cerr << "Vp: " << vp_ << ", gp: " << gp_ << std::endl;

    for (size_t i = 0; i < bitrates_.size(); ++i) {
      std::cerr << i << "    " << i << "/-: " << BufferLevelForZeroScore(i);
      if (i > 0) {
        std::cerr << "    " << i << "/" << (i - 1) << ": "
                  << MinBufferLevelForQuality(i);
      }
      std::cerr << std::endl;
      if (false) {
        for (double j = 0.0; j < 32500; j += 4000) {
          std::cerr << j << " "
                    << ((vp_ * (utilities_[i] + gp_) - j) / bitrates_[i])
                    << std::endl;
        }
      }
    }
  }
}


int BolaAbr::GetQuality(double throughput, double* pause)
{
  int quality = 0;
  switch (variant_) {
    case kBolaB:
      quality = BolaB(this->buffer_level_, throughput, pause);
      break;
    case kBolaU:
    case kBolaO:
      quality = BolaUO(this->buffer_level_, throughput, pause);
      break;
    case kBolaE:
      quality = BolaE(this->buffer_level_, throughput, pause);
      break;
  }
  AbrLogLine log_line;
  if (log_.empty()) {
    log_line.playhead_time_ = -this->buffer_level_;
  } else {
    log_line.playhead_time_ = log_.back().playhead_time_ + segment_duration_ +
      log_.back().buffer_level_ - this->buffer_level_;
  }
  log_line.buffer_level_ = this->buffer_level_;
  log_line.throughput_ = throughput;
  log_line.quality_ = quality;
  log_line.bitrate_ = bitrates_[quality];
  log_line.pause_ = *pause;
  log_.push_back(log_line);

  this->pause = *pause;

  return quality;
}

const std::vector<AbrLogLine>& BolaAbr::GetLog()
{
  return log_;
}

int BolaAbr::BolaB(double buffer_level, double throughput, double* pause)
{
  int quality = 0;
  if (buffer_level + segment_duration_ > buffer_size_) {
    *pause = buffer_level + segment_duration_ - buffer_size_;
  } else {
    *pause = 0.0;
  }
  quality = QualityFromBufferLevel(buffer_level);
  last_quality_ = quality;
  return quality;
}

int BolaAbr::BolaUO(double buffer_level, double throughput, double* pause)
{
  int quality = 0;
  if (buffer_level + segment_duration_ > buffer_size_) {
    *pause = buffer_level + segment_duration_ - buffer_size_;
  } else {
    *pause = 0.0;
  }

  quality = QualityFromBufferLevel(buffer_level);
  if (quality <= last_quality_ || bitrates_[quality] <= throughput) {
    last_quality_ = quality;
    return quality;
  }

  // we have a quality increase to an unsustainable bitrate

  int sustainable_quality = QualityFromThroughput(throughput);
  if (sustainable_quality < last_quality_) {
    return last_quality_;
  }

  if (variant_ == kBolaO) {
    quality = sustainable_quality;
    double level = BufferLevelForZeroScore(quality);
    if (buffer_level > level) {
      *pause = buffer_level - level;
    }
    last_quality_ = quality;
    return quality;
  } else { // variant_ == kBolaU
    quality = sustainable_quality + 1;
    last_quality_ = quality;
    return quality;
  }
}

int BolaAbr::BolaE(double buffer_level, double throughput, double* pause)
{
  int quality = 0;
  *pause = 0.0;

  if (last_quality_ == -1) {
    ibr_safety_factor_ = 1.0;
    ibr_safety_factor_target_ = segment_duration_ / buffer_size_;
    if (ibr_safety_factor_target_ < kIbrSafetyFactor) {
      ibr_safety_factor_target_ = kIbrSafetyFactor;
    } else if (ibr_safety_factor_target_ > kSafetyFactor) {
      ibr_safety_factor_target_ = kSafetyFactor;
    }
    quality = QualityFromThroughput(kSafetyFactor * throughput);
    placeholder_ = MinBufferLevelForQuality(quality);
    last_quality_ = quality;
    return quality;
  }

  ibr_safety_factor_ *= kSafetyFactor;
  if (ibr_safety_factor_ < ibr_safety_factor_target_) {
    ibr_safety_factor_ = ibr_safety_factor_target_;
  }

  quality = QualityFromBufferLevel(buffer_level + placeholder_);
  int safe_quality = InsufficientBufferRule(buffer_level, throughput);
  int sustainable_quality = QualityFromThroughput(throughput);

  if (quality > safe_quality) {
    quality = safe_quality;
  }

  if (quality > last_quality_ || quality > sustainable_quality) {
    if (sustainable_quality < last_quality_) {
      quality = last_quality_;
    } else {
      quality = sustainable_quality;
    }
  }

  double level = BufferLevelForZeroScore(quality);
  if (buffer_level + placeholder_ > level) {
    placeholder_ = level - buffer_level;
    if (placeholder_ < 0.0) {
      *pause = -placeholder_;
      placeholder_ = 0.0;
    }
  }

  last_quality_ = quality;
  return quality;
}

int BolaAbr::Score(int quality, double buffer_level)
{
  return (vp_ * (utilities_[quality] + gp_) - buffer_level) /
      bitrates_[quality];
}

double BolaAbr::BufferLevelForZeroScore(int quality)
{
  return vp_ * (utilities_[quality] + gp_);
}

double BolaAbr::MinBufferLevelForQuality(int quality)
{
  if (quality == 0) {
    return 0.0;
  }

  double a = (bitrates_[quality - 1] * utilities_[quality] -
              bitrates_[quality] * utilities_[quality - 1])
      / (bitrates_[quality] - bitrates_[quality - 1]);
  return vp_ * (gp_ - a);
}

int BolaAbr::QualityFromBufferLevel(double buffer_level)
{
  int quality = 0;
  double score = 0.0;
  for (size_t i = 0; i < bitrates_.size(); ++i) {
    double s = Score(i, buffer_level);
    if (i == 0 || s >= score) {
      score = s;
      quality = i;
    }
  }
  return quality;
}

int BolaAbr::QualityFromThroughput(double throughput)
{
  for (size_t i = 1; i < bitrates_.size(); ++i) {
    if (bitrates_[i] > throughput) {
      return i - 1;
    }
  }
  return bitrates_.size() - 1;
}

int BolaAbr::InsufficientBufferRule(double buffer_level, double throughput)
{
  for (size_t i = 1; i < bitrates_.size(); ++i) {
    if (bitrates_[i] >
        throughput * ibr_safety_factor_ * buffer_level / segment_duration_) {
      return i - 1;
    }
  }
  return bitrates_.size() - 1;
}

void BolaAbr::PostUpdate(double pause, uint32_t walltime) {
  if (pause > 0.0)
    buffer_level_ -= pause;

  buffer_level_ -= walltime;

  if (buffer_level_ < 0.0) {
    std::cerr << "Rebuffering for " << (-buffer_level_) << " ms" << std::endl;
    buffer_level_ = 0.0;
  }

  buffer_level_ += segment_duration_;

  std::cerr << "debug: new buf level: " << buffer_level_ << " | walltime: " << walltime << std::endl;
} 

/*
int main()
{
  std::vector<double> bitrates = { 1000, 2000, 4000, 8000, 16000};
  TrivialBandwidthModel bandwidth_model;
  ThroughputEstimator throughput_estimator;
  BolaAbr bola(kBolaE, bitrates, 5000, 25000);

  double buffer_level = 0.0;
  for (int i = 0; i < 100; ++i) 
  {

    int quality = 0;
    double pause = 0.0;
    if (i == 0) 
    {
      // first segment at low quality
      quality = 0;
    } 
    else 
    {
      double throughput_estimate = throughput_estimator.GetThroughput();
      quality = bola.GetQuality(buffer_level, throughput_estimate, &pause);
    }
    std::cerr << "buffer_level = " << buffer_level << std::endl;
    if (pause > 0.0) 
    {
      buffer_level -= pause;
      std::cerr << "BOLA: pause " << pause << " ms, quality = " << quality
                << std::endl;
      bandwidth_model.AddTime(pause);
    } 
    else 
    {
      std::cerr << "ABR: quality = " << quality << std::endl;
    }
    double bandwidth = bandwidth_model.GetBandwidth();
    double time = 5000 * bitrates[quality] / bandwidth;
    std::cerr << "Downloading at quality " << quality << " ("
              << bitrates[quality] << " kbps), time = " << time << " ms"
              << std::endl;
    throughput_estimator.AddMeasurement(bandwidth, time);
    std::cerr << "AddMeasurement(" << bandwidth << ", " << time
              << "); GetThroughput() == "
              << throughput_estimator.GetThroughput() << std::endl;
    bandwidth_model.AddTime(time);
    buffer_level -= time;
    if (buffer_level < 0.0) {
      if (i > 0) {
        std::cerr << "Rebuffering for " << (-buffer_level) << std::endl;
      }
      buffer_level = 0.0;
    }

    buffer_level += 5000;
  }
}

*/
