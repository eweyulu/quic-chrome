#ifndef BOLA_ABR
#define BOLA_ABR

#include <vector>
#include <iostream>
#include <cmath>

// time in ms
// size in bits
// bitrate in kbps (equivalent to bits/ms)

class BaseAbr;
class AbstractDispatcher;

enum BolaVariant {
  kBolaB = 0,
  kBolaU,
  kBolaO,
  kBolaE, // RECOMMENDED
};

class BolaAbr: public BaseAbr {
 public:
  ~BolaAbr();
  BolaAbr(BolaVariant variant,
          double segment_duration,
          double buffer_size,
          std::vector<double> bitrates);

  int GetQuality(double throughput, double* pause);
  void PostUpdate(double pause, uint32_t);
  double accept(AbstractDispatcher &dispatcher) override;
  const std::vector<AbrLogLine>& GetLog();

  int pause;

  //private:
    double buffer_level_;
    int last_quality_;
    double placeholder_;
    double vp_;
    double gp_;
    BolaVariant variant_;
    double segment_duration_;
    double buffer_size_;
    double ibr_safety_factor_target_;
    double ibr_safety_factor_;
    std::vector<double> bitrates_;
    std::vector<double> utilities_;
    std::vector<AbrLogLine> log_;
    static constexpr double kBufferLow = 10000;
    static constexpr double kMinThreshold = 2000;
    static constexpr double kSafetyFactor = 0.9;
    static constexpr double kIbrSafetyFactor = 0.5;
    int BolaB(double buffer_level, double throughput, double* pause);
    int BolaUO(double buffer_level, double throughput, double* pause);
    int BolaE(double buffer_level, double throughput, double* pause);
    int Score(int quality, double buffer_level);
    double BufferLevelForZeroScore(int quality);
    double MinBufferLevelForQuality(int quality);
    int QualityFromBufferLevel(double buffer_level);
    int QualityFromThroughput(double throughput);
    int InsufficientBufferRule(double buffer_level, double throughput);
};

#endif //BOLA_ABR
