#include "net/tools/quic/abr.h"
#include "net/tools/quic/mpc.h"

MpcAbr::~MpcAbr() {
}

double MpcAbr::accept(AbstractDispatcher &dispatcher) {
    return dispatcher.GetQuality(*this);
}

MpcAbr::MpcAbr(double segment_duration, double buffer_size,
               std::vector<double> bitrates)
    : buffer_level_(0.0),
      last_quality_(0),
      segment_duration_(segment_duration),
      buffer_size_(buffer_size),
      bitrates_(bitrates),
      estimate_throughput_(0.0),
      estimate_error_(0.0)
{
}

int MpcAbr::GetQuality(double throughput, double* pause)
{
  double best = 0.0;
  int quality = 0;
  double tput_e = throughput / (1.0 + estimate_error_);
  for (unsigned q = 0; q < bitrates_.size(); ++q) {
    double v = Search(kSearchDepth, tput_e, buffer_level_, last_quality_, q);
    if (q == 0 || v > best) {
      best = v;
      quality = q;
    }
  }
  last_quality_ = quality;
  estimate_throughput_ = throughput;

  *pause = 0.0;
  AbrLogLine log_line;
  if (log_.empty()) {
    log_line.playhead_time_ = -this->buffer_level_;
  } else {
    log_line.playhead_time_ = log_.back().playhead_time_ + segment_duration_ +
      log_.back().buffer_level_ - this->buffer_level_;
  }
  log_line.buffer_level_ = this->buffer_level_;
  log_line.throughput_ = throughput;
  log_line.quality_ = quality;
  log_line.bitrate_ = bitrates_[quality];
  log_line.pause_ = *pause;
  log_.push_back(log_line);

  this->pause = *pause;
  return quality;
}

const std::vector<AbrLogLine>& MpcAbr::GetLog()
{
  return log_;
}

double MpcAbr::Evaluate(int prev_quality, int quality, double rebuffer)
{
  const double lambda = 1.0;
  const double mu = 3.0;
  double score = bitrates_[quality];
  score -= lambda * fabs(bitrates_[quality] - bitrates_[prev_quality]);
  score -= mu * rebuffer;
  return score;
}

double MpcAbr::Search(int depth, double throughput, double buffer_level,
                      int prev_quality, int quality)
{
  double time = (bitrates_[quality] * segment_duration_) / throughput;
  double rebuffer = 0.0;
  buffer_level -= time;
  if (buffer_level < 0.0) {
    rebuffer += -buffer_level;
    buffer_level = 0.0;
  }
  buffer_level += segment_duration_;

  double value = Evaluate(prev_quality, quality, rebuffer);
  --depth;
  if (depth > 0) {
    double best = 0.0;
    for (unsigned q = 0; q < bitrates_.size(); ++q) {
      double v = Search(depth, throughput, buffer_level, quality, q);
      if (q == 0 || v > best) {
        best = v;
      }
    }
    value += best;
  }
  return value;
}

void MpcAbr::PostUpdate(double pause, uint32_t walltime, double segment_size) {
  if (pause > 0.0)
    buffer_level_ -= pause;

  buffer_level_ -= walltime;

  double throughput = segment_size / walltime;
  double error = std::fabs(estimate_throughput_ - throughput) / throughput;
  past_errors_.push_back(error);
  if (past_errors_.size() > kErrorWindow) {
    past_errors_.pop_front();
  }
  estimate_error_ = 0.0;
  for (auto error: past_errors_) {
    if (error > estimate_error_) {
      estimate_error_ = error;
    }
  }

  if (buffer_level_ < 0.0) {
    std::cerr << "Rebuffering for " << (-buffer_level_) << " ms" << std::endl;
    buffer_level_ = 0.0;
  }

  buffer_level_ += segment_duration_;

  std::cerr << "debug: new buf level: " << buffer_level_ << " | walltime: " << walltime << std::endl;
}
