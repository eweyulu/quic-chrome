// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// A binary wrapper for QuicClient.
// Connects to a host using QUIC, sends a request to the provided URL, and
// displays the response.
//
// Some usage examples:
//
//   TODO(rtenneti): make --host optional by getting IP Address of URL's host.
//
//   Get IP address of the www.google.com
//   IP=`dig www.google.com +short | head -1`
//
// Standard request/response:
//   quic_client http://www.google.com  --host=${IP}
//   quic_client http://www.google.com --quiet  --host=${IP}
//   quic_client https://www.google.com --port=443  --host=${IP}
//
// Use a specific version:
//   quic_client http://www.google.com --quic_version=23  --host=${IP}
//
// Send a POST instead of a GET:
//   quic_client http://www.google.com --body="this is a POST body" --host=${IP}
//
// Append additional headers to the request:
//   quic_client http://www.google.com  --host=${IP}
//               --headers="Header-A: 1234; Header-B: 5678"
//
// Connect to a host different to the URL being requested:
//   Get IP address of the www.google.com
//   IP=`dig www.google.com +short | head -1`
//   quic_client mail.google.com --host=${IP}
//
// Try to connect to a host which does not speak QUIC:
//   Get IP address of the www.example.com
//   IP=`dig www.example.com +short | head -1`
//   quic_client http://www.example.com --host=${IP}

//#define SLST_DEBUG 1

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <string>
#include <string.h>
#include <dirent.h>

#include "base/at_exit.h"
#include "base/command_line.h"
#include "base/logging.h"
#include "base/message_loop/message_loop.h"
#include "base/task/task_scheduler/task_scheduler.h"
#include "net/base/net_errors.h"
#include "net/base/privacy_mode.h"
#include "net/cert/cert_verifier.h"
#include "net/cert/ct_log_verifier.h"
#include "net/cert/ct_policy_enforcer.h"
#include "net/cert/multi_log_ct_verifier.h"
#include "net/http/transport_security_state.h"
#include "net/quic/crypto/proof_verifier_chromium.h"
#include "net/spdy/spdy_http_utils.h"
#include "net/third_party/quic/core/quic_error_codes.h"
#include "net/third_party/quic/core/quic_packets.h"
#include "net/third_party/quic/core/quic_server_id.h"
#include "net/third_party/quic/platform/api/quic_socket_address.h"
#include "net/third_party/quic/platform/api/quic_str_cat.h"
#include "net/third_party/quic/platform/api/quic_string_piece.h"
#include "net/third_party/quic/platform/api/quic_text_utils.h"
#include "net/third_party/spdy/core/spdy_header_block.h"
#include "net/tools/quic/quic_simple_client.h"
#include "net/tools/quic/synchronous_host_resolver.h"
#include "url/gurl.h"
#include "net/third_party/quic/core/quic_types.h"


#include "third_party/libxml/chromium/libxml_utils.h"

#include "abr.h"
#include "bola.h"
#include "mpc.h"

using net::CertVerifier;
using net::CTVerifier;
using net::MultiLogCTVerifier;
using quic::ProofVerifier;
using net::ProofVerifierChromium;
using quic::QuicStringPiece;
using quic::QuicTextUtils;
using net::TransportSecurityState;
using spdy::SpdyHeaderBlock;
using std::cout;
using std::cerr;
using  std::endl; 
using std::string;

// The IP or hostname the quic client will connect to.
string FLAGS_host = "";
// The port to connect to.
int32_t FLAGS_port = 0;
// If set, send a POST with this body.
string FLAGS_body = "";
// If set, contents are converted from hex to ascii, before sending as body of
// a POST. e.g. --body_hex=\"68656c6c6f\"
string FLAGS_body_hex = "";
// A semicolon separated list of key:value pairs to add to request headers.
string FLAGS_headers = "";
// Set to true for a quieter output experience.
bool FLAGS_quiet = false;
// QUIC version to speak, e.g. 21. If not set, then all available versions are
// offered in the handshake.
int32_t FLAGS_quic_version = -1;
// If true, a version mismatch in the handshake is not considered a failure.
// Useful for probing a server to determine if it speaks any version of QUIC.
bool FLAGS_version_mismatch_ok = false;
// If true, an HTTP response code of 3xx is considered to be a successful
// response, otherwise a failure.
bool FLAGS_redirect_is_success = true;
// Initial MTU of the connection.
int32_t FLAGS_initial_mtu = 0;
// buffer length in ms for ABR
int32_t FLAGS_abr_buf = 20000;

std::string FLAGS_ref_video = "";


class FakeProofVerifier : public quic::ProofVerifier {
 public:
  quic::QuicAsyncStatus VerifyProof(
      const string& hostname,
      const uint16_t port,
      const string& server_config,
      quic::QuicTransportVersion quic_version,
      quic::QuicStringPiece chlo_hash,
      const std::vector<string>& certs,
      const string& cert_sct,
      const string& signature,
      const quic::ProofVerifyContext* context,
      string* error_details,
      std::unique_ptr<quic::ProofVerifyDetails>* details,
      std::unique_ptr<quic::ProofVerifierCallback> callback) override {
    return quic::QUIC_SUCCESS;
  }

  quic::QuicAsyncStatus VerifyCertChain(
      const std::string& hostname,
      const std::vector<std::string>& certs,
      const quic::ProofVerifyContext* verify_context,
      std::string* error_details,
      std::unique_ptr<quic::ProofVerifyDetails>* verify_details,
      std::unique_ptr<quic::ProofVerifierCallback> callback) override {
    return quic::QUIC_SUCCESS;
  }

  std::unique_ptr<quic::ProofVerifyContext> CreateDefaultContext() override {
    return nullptr;
  }
};

typedef struct {
  char type;
  uint32_t number;
  size_t length;

  std::string print();
} VideoFrames;

std::string VideoFrames::print() {
  return "type: " + std::string(1, type) + " num: " + std::to_string(number) + " size: " + std::to_string(length) + "Byte";
}

std::string quic::FrameTiming::print() {
  return std::to_string(qt.ToDebuggingValue()) + " size: " + std::to_string(length) + " lost? " + ((was_lost)?"true":"false");
}

std::map < uint32_t, VideoFrames >::iterator video_frame_it;
quic::QuicTime total_time = quic::QuicTime::Zero();

void print_frame_timings( std::map < uint32_t, VideoFrames > *vframes, std::map < quic::QuicStreamOffset, quic::FrameTiming > *response_timings, uint32_t* total_bytes )
{
  std::map < quic::QuicStreamOffset, quic::FrameTiming >::iterator quic_frame_it = response_timings->begin();
  
  quic::QuicTime receipt_time = quic::QuicTime::Zero();
  quic::QuicTime ratime = quic_frame_it->second.qt;
  quic::QuicTime last_time = quic_frame_it->second.qt;
  size_t lost_bytes = 0;

  for (; quic_frame_it != response_timings->end(); last_time = quic_frame_it->second.qt, ++quic_frame_it)
  {
    receipt_time = receipt_time + (quic_frame_it->second.qt - last_time);

      /*
      std::cerr << "lost? " << quic_frame_it->second.was_lost 
                << " start time: " <<  quic_frame_it->second.qt - last_time
                << "\nvideo_frame: pos: " << video_frame_it->first << " len: " << video_frame_it->second.length
                << "\nquic_frame: abs-pos: " << (*total_bytes) << " pos: " << quic_frame_it->first << " len: " << quic_frame_it->second.length <<"\n" << std::endl;
      */

      *total_bytes += quic_frame_it->second.length;
      lost_bytes += (quic_frame_it->second.was_lost) ? quic_frame_it->second.length : 0;

      if ((*total_bytes) >= video_frame_it->first + video_frame_it->second.length)
      {
        std::cerr << "Frame: " << video_frame_it->second.print() << " off: " << video_frame_it->first << "Byte atime: " << (quic_frame_it->second).qt.ToDebuggingValue()  << " ratime: " << ((quic_frame_it->second).qt - ratime) << " recv: " << ((lost_bytes == video_frame_it->second.length)?(quic::QuicTime::Zero()-quic::QuicTime::Zero()):(receipt_time - quic::QuicTime::Zero())) << " lost-bytes: " << lost_bytes << "Byte" << std::endl;
        total_time = total_time + (receipt_time - quic::QuicTime::Zero());
        ++video_frame_it;
        lost_bytes = 0;
        receipt_time = quic::QuicTime::Zero();
      }
  }
}




class Transport : public TransportInterface {

  public:
    Transport(net::QuicSimpleClient* client) : client_(client) {}

    double GetThroughput() override {

      ma.AddMeasurement(client_->GetThroughput(), client_->GetTime());
      return ma.GetThroughput();

      //return client_->GetThroughput();
    }
    uint32_t GetTime() override {
      return client_->GetTime();
    }

    bool GetInTime() override {
      return client_->GetInTime();
    }

    double GetSegmentSize() override {
      return client_->GetSegmentSize();
    }

  protected:
    net::QuicSimpleClient* client_;
  private:
    MovingAverage ma;
};

class TransportFine : public Transport {
  public:
    TransportFine(net::QuicSimpleClient* client) : Transport(client) {
      client_->SetFine(true);
      client_->SetOnlyIFrame(false);
    }
};

class TransportFineOnlyIFrame : public Transport {
  public:
    TransportFineOnlyIFrame(net::QuicSimpleClient* client) : Transport(client) {
      client_->SetFine(true);
      client_->SetOnlyIFrame(true);
    }
};

/*
class TransportFineRaw : public TransportInterface {

  public:
    Transport(net::QuicSimpleClient* client) : client_(client) {}

    double GetThroughput() override {
      return client_->GetThroughputFine();
    }
    uint32_t GetTime() override {
      return client_->GetTime();
    }
  private:
    net::QuicSimpleClient* client_;
    MovingAverage ma;
};
*/


///////////////////



//MAIN



///////////////////



int main(int argc, char* argv[]) {

  base::CommandLine::Init(argc, argv);
  base::CommandLine* line = base::CommandLine::ForCurrentProcess();
  const base::CommandLine::StringVector& urls = line->GetArgs();
  base::TaskScheduler::CreateAndStartWithDefaultParams("quic_client");

  logging::LoggingSettings settings;
  settings.logging_dest = logging::LOG_TO_SYSTEM_DEBUG_LOG;
  CHECK(logging::InitLogging(settings));

  if (line->HasSwitch("h") || line->HasSwitch("help") || urls.empty()) {
    const char* help_str =
        "Usage: quic_client [options] <url>\n"
        "\n"
        "<url> with scheme must be provided (e.g. http://www.google.com)\n\n"
        "Options:\n"
        "-h, --help                  show this help message and exit\n"
        "--host=<host>               specify the IP address of the hostname to "
        "connect to\n"
        "--port=<port>               specify the port to connect to\n"
        "--body=<body>               specify the body to post\n"
        "--body_hex=<body_hex>       specify the body_hex to be printed out\n"
        "--headers=<headers>         specify a semicolon separated list of "
        "key:value pairs to add to request headers\n"
        "-q, --quiet                 specify for a quieter output experience\n"
        "--quic-version=<quic version> specify QUIC version to speak\n"
        "--version_mismatch_ok       if specified a version mismatch in the "
        "handshake is not considered a failure\n"
        "--redirect_is_success       if specified an HTTP response code of 3xx "
        "is considered to be a successful response, otherwise a failure\n"
        "--initial_mtu=<initial_mtu> specify the initial MTU of the connection"
        "\n"
        "--disable-certificate-verification do not verify certificates\n"
        "--ref_video=<mp4frames>     specify reference video frame information for frame timings\n"
        "--abr_buf=<ms>              specify the amount (in ms) of buffer for the ABR to use\n";
    cerr << help_str;
    exit(0);
  }
  if (line->HasSwitch("host")) {
    FLAGS_host = line->GetSwitchValueASCII("host");
  }
  if (line->HasSwitch("port")) {
    if (!base::StringToInt(line->GetSwitchValueASCII("port"), &FLAGS_port)) {
      std::cerr << "--port must be an integer\n";
      return 1;
    }
  }
  if (line->HasSwitch("body")) {
    FLAGS_body = line->GetSwitchValueASCII("body");
  }
  if (line->HasSwitch("body_hex")) {
    FLAGS_body_hex = line->GetSwitchValueASCII("body_hex");
  }
  if (line->HasSwitch("headers")) {
    FLAGS_headers = line->GetSwitchValueASCII("headers");
  }
  if (line->HasSwitch("q") || line->HasSwitch("quiet")) {
    FLAGS_quiet = true;
  }
  if (line->HasSwitch("quic-version")) {
    int quic_version;
    if (base::StringToInt(line->GetSwitchValueASCII("quic-version"),
                          &quic_version)) {
      FLAGS_quic_version = quic_version;
    }
  }
  if (line->HasSwitch("version_mismatch_ok")) {
    FLAGS_version_mismatch_ok = true;
  }
  if (line->HasSwitch("redirect_is_success")) {
    FLAGS_redirect_is_success = true;
  }
  if (line->HasSwitch("initial_mtu")) {
    if (!base::StringToInt(line->GetSwitchValueASCII("initial_mtu"),
                           &FLAGS_initial_mtu)) {
      std::cerr << "--initial_mtu must be an integer\n";
      return 1;
    }
  }
  if (line->HasSwitch("ref_video")) {
    FLAGS_ref_video = line->GetSwitchValueASCII("ref_video");
  }
  if (line->HasSwitch("abr_buf")) {
    if (!base::StringToInt(line->GetSwitchValueASCII("abr_buf"),
                           &FLAGS_abr_buf)) {
      std::cerr << "--abr_buf must be an integer\n";
      return 1;
    }
  }

  VLOG(1) << "server host: " << FLAGS_host << " port: " << FLAGS_port
          << " body: " << FLAGS_body << " headers: " << FLAGS_headers
          << " quiet: " << FLAGS_quiet
          << " quic-version: " << FLAGS_quic_version
          << " version_mismatch_ok: " << FLAGS_version_mismatch_ok
          << " redirect_is_success: " << FLAGS_redirect_is_success
          << " initial_mtu: " << FLAGS_initial_mtu;

  base::AtExitManager exit_manager;
  base::MessageLoopForIO message_loop;

  // Determine IP address to connect to from supplied hostname.
  quic::QuicIpAddress ip_addr;

  GURL url(urls[0]);
  string host = FLAGS_host;
  if (host.empty()) {
    host = url.host();
  }
  int port = FLAGS_port;
  if (port == 0) {
    port = url.EffectiveIntPort();
  }
  if (!ip_addr.FromString(host)) {
    net::AddressList addresses;
    int rv = net::SynchronousHostResolver::Resolve(host, &addresses);
    if (rv != net::OK) {
      LOG(ERROR) << "Unable to resolve '" << host
                 << "' : " << net::ErrorToShortString(rv);
      return 1;
    }
    ip_addr =
        quic::QuicIpAddress(quic::QuicIpAddressImpl(addresses[0].address()));
  }

  string host_port = quic::QuicStrCat(ip_addr.ToString(), ":", port);
  VLOG(1) << "Resolved " << host << " to " << host_port << endl;

  // Build the client, and try to connect.
  quic::QuicServerId server_id(url.host(), url.EffectiveIntPort(),
                               net::PRIVACY_MODE_DISABLED);
  quic::ParsedQuicVersionVector versions = quic::CurrentSupportedVersions();
  if (FLAGS_quic_version != -1) {
    versions.clear();
    versions.push_back(quic::ParsedQuicVersion(
        quic::PROTOCOL_QUIC_CRYPTO,
        static_cast<quic::QuicTransportVersion>(FLAGS_quic_version)));
  }
  // For secure QUIC we need to verify the cert chain.
  std::unique_ptr<CertVerifier> cert_verifier(CertVerifier::CreateDefault());
  std::unique_ptr<TransportSecurityState> transport_security_state(
      new TransportSecurityState);
  std::unique_ptr<MultiLogCTVerifier> ct_verifier(new MultiLogCTVerifier());
  std::unique_ptr<net::CTPolicyEnforcer> ct_policy_enforcer(
      new net::DefaultCTPolicyEnforcer());
  std::unique_ptr<quic::ProofVerifier> proof_verifier;
  if (line->HasSwitch("disable-certificate-verification")) {
    proof_verifier.reset(new FakeProofVerifier());
  } else {
    proof_verifier.reset(new ProofVerifierChromium(
        cert_verifier.get(), ct_policy_enforcer.get(),
        transport_security_state.get(), ct_verifier.get()));
  }
  net::QuicSimpleClient client(quic::QuicSocketAddress(ip_addr, port),
                               server_id, versions, std::move(proof_verifier));
  client.set_initial_max_packet_length(
      FLAGS_initial_mtu != 0 ? FLAGS_initial_mtu : quic::kDefaultMaxPacketSize);
  if (!client.Initialize()) {
    cerr << "Failed to initialize client." << endl;
    return 1;
  }
  if (!client.Connect()) {
    quic::QuicErrorCode error = client.session()->error();
    if (FLAGS_version_mismatch_ok && error == quic::QUIC_INVALID_VERSION) {
      cerr << "Server talks QUIC, but none of the versions supported by "
           << "this client: " << ParsedQuicVersionVectorToString(versions)
           << endl;
      // Version mismatch is not deemed a failure.
      return 0;
    }
    cerr << "Failed to connect to " << host_port
         << ". Error: " << quic::QuicErrorCodeToString(error) << endl;
    return 1;
  }
  if (!FLAGS_quiet)
    cerr << "Connected to " << host_port << "\n\n\n\n\n\n\n\n\n" <<  endl;

  // Construct the string body from flags, if provided.
  string body = FLAGS_body;
  if (!FLAGS_body_hex.empty()) {
    DCHECK(FLAGS_body.empty()) << "Only set one of --body and --body_hex.";
    body = quic::QuicTextUtils::HexDecode(FLAGS_body_hex);
  }

  // Construct a GET or POST request for supplied URL.
  SpdyHeaderBlock header_block;
  header_block[":method"] = body.empty() ? "GET" : "POST";
  header_block[":scheme"] = url.scheme();
  header_block[":authority"] = url.host();
  header_block[":path"] = url.path();

  // Append any additional headers supplied on the command line.
  for (quic::QuicStringPiece sp :
       quic::QuicTextUtils::Split(FLAGS_headers, ';')) {
    quic::QuicTextUtils::RemoveLeadingAndTrailingWhitespace(&sp);
    if (sp.empty()) {
      continue;
    }
    std::vector<quic::QuicStringPiece> kv = quic::QuicTextUtils::Split(sp, ':');
    quic::QuicTextUtils::RemoveLeadingAndTrailingWhitespace(&kv[0]);
    quic::QuicTextUtils::RemoveLeadingAndTrailingWhitespace(&kv[1]);
    header_block[kv[0]] = kv[1];
  }

  // Make sure to store the response, for later output.
  client.set_store_response(true);


  //std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  // Print request and response details.
  if (!FLAGS_quiet) {
    cerr << "MANIFEST Request:" << endl;
    cerr << "headers:" << header_block.DebugString();
    cerr << "body: " << body << endl;
  }

  // Send the request for manifest
  client.SendRequestAndWaitForResponse(header_block, body, /*fin=*/true, /*unrel*/false);
  #ifdef SLST_DEBUG 
 std::cerr  << " ---- \n\n\n\n" <<  std::endl; 
 #endif

  if (!FLAGS_quiet) {
    cerr << "Response:" << endl;
    cerr << "headers: " << client.latest_response_headers() << endl;
  }
  string response_body = client.latest_response_body();
  if (!FLAGS_body_hex.empty()) {
    // Assume response is binary data.
    if (!FLAGS_quiet)
    cerr << "body:\n" << quic::QuicTextUtils::HexDump(response_body) << endl;
  } else {
    if (!FLAGS_quiet)
    cerr << "body: " << response_body << endl;
  }
  if (!FLAGS_quiet)
  cerr << "trailers: " << client.latest_response_trailers() << "\n\n" << endl;

  //exit(-1);


/*
  size_t response_code = client.latest_response_code();
  if (response_code >= 200 && response_code < 300) {
    cerr << "Request succeeded (" << response_code << ")." << endl;
    return 0;
  } else if (response_code >= 300 && response_code < 400) {
    if (FLAGS_redirect_is_success) {
      cerr << "Request succeeded (redirect " << response_code << ")." << endl;
      return 0;
    } else {
      cerr << "Request failed (redirect " << response_code << ")." << endl;
      return 1;
    }
  } else {
    cerr << "Request failed (" << response_code << ")." << endl;
    return 1;
  }
  */


  uint32_t total_bytes = 0;
  
  //std::fstream myfile; 
  //std::fstream mycombinedfile = std::fstream("v3_big_buck_inter_5000k_super_short_RECEIVED.mp4", std::ios::out | std::ios::binary);
  //int file_cnt = 0;
  ////std::ofstream mycombinedfile = std::cout.rdbuf();

  std::string xml_body(response_body);
  XmlReader xml_reader;
  if (!xml_reader.Load(xml_body))
  {
    cerr << "COULD NOT READ XML" << endl;
    return -1;
  }
  else {
  //  cerr << xml_body << endl;
  }

  std::map < std::string, std::map < uint32_t, VideoFrames > > vframes;

  DIR *dir;
  struct dirent *entry;

  if ((dir = opendir(FLAGS_ref_video.c_str())) != NULL) 
  {
    while ((entry = readdir(dir)) != NULL)
    {
      string s(entry->d_name);
      if (s.find(".csv") != string::npos)
      {
        //frames->insert( std::pair<uint32_t, VideoFrames>(0, {'H', 0, current_read_pos - 4}));

        std::map < uint32_t, VideoFrames > frame;

        std::ifstream infile(s);
        std::string dummy;
        std::getline(infile, dummy);

        uint32_t byte_pos = 0;

        char type;
        int num;
        int size;

        while (infile >> type >> num >> size)
        {
          frame.insert(std::pair<uint32_t, VideoFrames>(byte_pos, {type, num, size}));
        }
        vframes.insert(std::pair<std::string, std::map < uint32_t, VideoFrames > >(s, frame));
      }
    }
  }


  //ParseMp4(FLAGS_ref_video, &vframes);
  //video_frame_it = vframes.begin();


  size_t total_written = 0;
  std::map< quic::QuicStreamOffset, quic::FrameTiming > response_timings;

  typedef struct {
    std::string mediaRange;
    std::string reliableRange;
    std::string unreliableRange;
  } segment;

  typedef struct {
    std::string baseUrl;
    std::vector<segment> segments;
  } repr;

  std::map< uint32_t, repr > adaptationSet;
  uint32_t current_repr_bw = 0;


  while (xml_reader.Read()) 
  {
    xml_reader.SkipToElement();
    std::string node_name(xml_reader.NodeName());

    if (node_name == "Representation" && !xml_reader.IsClosingElement()) {
      std::string type;
      xml_reader.NodeAttribute("mimeType", &type);

      if(type.rfind("audio", 0) == 0) {
        std::cerr << "found audio: ignoring for now" << std::endl;
      }

      std::cerr << "found repr" << std::endl;
      std::string bw;
      unsigned int ibw;

      xml_reader.NodeAttribute("bandwidth", &bw);
      ibw = std::stoi(bw, nullptr, 0);
      current_repr_bw = (uint32_t)(ibw / 1000);

      adaptationSet.insert(std::pair<uint32_t, repr>(current_repr_bw, {"", {}}));
    }
    else
    if (node_name == "BaseURL" && !xml_reader.IsClosingElement()) 
    {
      std::string file_name;
      xml_reader.ReadElementContent(&file_name);
      adaptationSet[current_repr_bw].baseUrl = file_name;
    }
    else 
    if (node_name == "Initialization" && !xml_reader.IsClosingElement())
    {
      std::string range;
      xml_reader.NodeAttribute("range", &range);
      adaptationSet[current_repr_bw].segments.push_back({range, range, ""});
    }
    else if (node_name == "SegmentURL" && !xml_reader.IsClosingElement())
    {
      std::string segment;
      std::string iEnd;
      xml_reader.NodeAttribute("mediaRange", &segment);
      xml_reader.NodeAttribute("iEnd", &iEnd);

      int st, en, i_marker;
      st = std::stoi( segment.substr(segment.find("=")+1, segment.find("-")), nullptr, 0 );
      en = std::stoi( segment.substr(segment.find("-")+1, segment.length()),  nullptr, 0 );

      std::string reliable_range;
      std::string unreliable_range;

      i_marker = std::stoi(iEnd, nullptr, 0);

      if (i_marker == st)
      {
        reliable_range.clear();
        unreliable_range = std::to_string(st) + "-" + std::to_string(en);
      }
      else if (i_marker == en)
      {
        reliable_range   = std::to_string(st) + "-" + std::to_string(en);
        unreliable_range.clear();
      }
      else
      {
        reliable_range   = std::to_string(st) + "-" + std::to_string(i_marker);
        unreliable_range = std::to_string(i_marker+1) + "-" + std::to_string(en);
      }

      adaptationSet[current_repr_bw].segments.push_back({segment, reliable_range, unreliable_range});
    }
  }

  std::vector<double> bitrates;
  //insert in reverse order so lowest quality (assuming an ordered list...) is first
  std::cerr << "bitrates: " << std::endl;
  for (auto& adap: adaptationSet) {
    std::cerr << adap.first << std::endl;
    bitrates.insert(bitrates.end(), adap.first);
  }

  BolaAbr bola(kBolaE, 4000, (double)(FLAGS_abr_buf), bitrates);
  TransportFine t(&client);

  Abr abr(&bola, &t);

  //download init segment first
  header_block[":path"] = "/" + adaptationSet[bitrates[0]].baseUrl;
  header_block[":range"] = string("bytes=") + adaptationSet[bitrates[0]].segments[0].mediaRange;

  cerr << "[chunk-marker]"  << adaptationSet[bitrates[0]].baseUrl << "|" << adaptationSet[bitrates[0]].segments[0].mediaRange << endl;

  response_body.clear();
  client.SendRequestAndWaitForResponse(header_block, /*request_body*/"", /*fin=*/true, /*unrel*/false);
  response_timings = client.latest_response_timings();
  video_frame_it = vframes[adaptationSet[bitrates[0]].baseUrl].begin();
  print_frame_timings(&vframes[adaptationSet[bitrates[0]].baseUrl], &response_timings, &total_bytes);
  response_body = client.latest_response_body();
  total_written += std::fwrite(response_body.data(), sizeof response_body[0], response_body.size(), stdout);
  fflush(stdout);


  //index 0 is initialization segment
  cerr << adaptationSet[bitrates[0]].segments.size() << endl;
  for (uint32_t i = 1; i < adaptationSet[bitrates[0]].segments.size(); ++i) {

    //double quality;
    int q;

    if (i == 1)
      q = 0;
    else 
    {
      q = (int) abr.GetQuality();

        int pause = (int) (bola.pause * 1000);
        usleep(pause);
        cerr << "sleeping for: " << pause << std::endl;
      
    }
    header_block[":path"] = "/" + adaptationSet[bitrates[q]].baseUrl;

    std::string rel   = adaptationSet[bitrates[q]].segments[i].reliableRange;
    std::string unrel = adaptationSet[bitrates[q]].segments[i].unreliableRange;

    cerr << "[chunk-marker]"  << adaptationSet[bitrates[q]].baseUrl << "|" << adaptationSet[bitrates[q]].segments[i].mediaRange << endl;

    cerr << "\n\ntime: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() << " requesting: " << adaptationSet[bitrates[q]].baseUrl << " rel: " << ((rel.length() == 0)?"nothing":rel) << " unrel: " << unrel << endl;

    if (rel != "") {
      header_block[":range"] = string("bytes=") + rel;
      response_body.clear();
      client.SendRequestAndWaitForResponse(header_block, /*request_body*/"", /*fin=*/true, /*unrel*/false);
      response_timings = client.latest_response_timings();
      video_frame_it = vframes[adaptationSet[bitrates[q]].baseUrl].begin();
      print_frame_timings(&vframes[adaptationSet[bitrates[q]].baseUrl], &response_timings, &total_bytes);
      response_body = client.latest_response_body();
      total_written += std::fwrite(response_body.data(), sizeof response_body[0], response_body.size(), stdout);
      fflush(stdout);
    }

    std::cerr << "---" << std::endl;

    

    if (unrel != "") {

      /*if (rel != "") {
        t.GetThroughput();
      }*/

      header_block[":range"] = string("bytes=") + unrel;
      response_body.clear();
      client.SendRequestAndWaitForResponse(header_block, /*request_body*/"", /*fin=*/true, /*unrel*/true);
      response_timings = client.latest_response_timings();
      video_frame_it = vframes[adaptationSet[bitrates[q]].baseUrl].begin();
      print_frame_timings(&vframes[adaptationSet[bitrates[q]].baseUrl], &response_timings, &total_bytes);
      response_body = client.latest_response_body();
      total_written += std::fwrite(response_body.data(), sizeof response_body[0], response_body.size(), stdout);
      fflush(stdout);
    }
  }

  auto log = bola.GetLog();
  bool start = true;
  for (auto x : log) {
    if (start) {
      start = false;
    } else {
      if (x.buffer_level_ <= 0.0) {
        std::cerr << "REBUFFERING" << std::endl;
      }
    }
    std::cerr << x.playhead_time_ << ": buf=" << x.buffer_level_ << ", tput="
              << x.throughput_ << ", bitrate[" << x.quality_ << "]="
              << x.bitrate_ << ", pause=" << x.pause_ << std::endl;
  }


  cerr << "total time: " << (total_time - quic::QuicTime::Zero()) << std::endl;
}
