#include "net/tools/quic/abr.h"
#include "net/tools/quic/tput.h"

ThroughputAbr::~ThroughputAbr() {
}

double ThroughputAbr::accept(AbstractDispatcher &dispatcher) {
    return dispatcher.GetQuality(*this);
}

ThroughputAbr::ThroughputAbr(double segment_duration,
                             double buffer_size,
                             std::vector<double> bitrates)
    : buffer_level_(0.0),
      segment_duration_(segment_duration),
      buffer_size_(buffer_size),
      bitrates_(bitrates)
{
}


int ThroughputAbr::GetQuality(double throughput, double* pause)
{
  int quality = QualityFromThroughput(throughput * kSafetyFactor);
  *pause = 0.0;
  AbrLogLine log_line;
  if (log_.empty()) {
    log_line.playhead_time_ = -this->buffer_level_;
  } else {
    log_line.playhead_time_ = log_.back().playhead_time_ + segment_duration_ +
      log_.back().buffer_level_ - this->buffer_level_;
  }
  log_line.buffer_level_ = this->buffer_level_;
  log_line.throughput_ = throughput;
  log_line.quality_ = quality;
  log_line.bitrate_ = bitrates_[quality];
  log_line.pause_ = *pause;
  log_.push_back(log_line);

  this->pause = *pause;
  return quality;
}

const std::vector<AbrLogLine>& ThroughputAbr::GetLog()
{
  return log_;
}

int ThroughputAbr::QualityFromThroughput(double throughput)
{
  for (size_t i = 1; i < bitrates_.size(); ++i) {
    if (bitrates_[i] > throughput) {
      return i - 1;
    }
  }
  return bitrates_.size() - 1;
}

void ThroughputAbr::PostUpdate(double pause, uint32_t walltime) {
  if (pause > 0.0)
    buffer_level_ -= pause;

  buffer_level_ -= walltime;

  if (buffer_level_ < 0.0) {
    std::cerr << "Rebuffering for " << (-buffer_level_) << " ms" << std::endl;
    buffer_level_ = 0.0;
  }

  buffer_level_ += segment_duration_;

  std::cerr << "debug: new buf level: " << buffer_level_ << " | walltime: " << walltime << std::endl;
}
