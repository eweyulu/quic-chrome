#ifndef TPUT_ABR
#define TPUT_ABR

#include <vector>
#include <iostream>
#include <cmath>

// time in ms
// size in bits
// bitrate in kbps (equivalent to bits/ms)

class BaseAbr;
class AbstractDispatcher;

class ThroughputAbr: public BaseAbr {
 public:
  ~ThroughputAbr();
  ThroughputAbr(double segment_duration,
                double buffer_size,
                std::vector<double> bitrates);

  int GetQuality(double throughput, double* pause);
  void PostUpdate(double pause, uint32_t);
  double accept(AbstractDispatcher &dispatcher) override;
  const std::vector<AbrLogLine>& GetLog();

  int pause;

  //private:
  double buffer_level_;
  double segment_duration_;
  double buffer_size_;
  std::vector<double> bitrates_;
  std::vector<AbrLogLine> log_;
  static constexpr double kSafetyFactor = 0.9;
  int QualityFromThroughput(double throughput);
};

#endif //TPUT_ABR
