#!/bin/bash
# This script assumes that the normal prepare.sh script was executed before,
# i.e., Chromium is set up and Slipstream modifications are applied.
if [ ! -d chrome/src ]
then
	echo "Folder 'chrome/src' does not exist. Make sure to run the prepare.sh before prepare-arm.sh"
	exit
fi

echo "NOTE: If this process fails, you might need to install the ARM build dependencies by running"
echo "  chrome/src/build/install-build-deps.sh --arm"
echo "but this requires root privileges."

cd chrome
export PATH="$PATH:$(pwd)/depot_tools"
cd src

echo "----------------------------------"
echo "      installing ARM sysroot"
echo "----------------------------------"

build/linux/sysroot_scripts/install-sysroot.py --arch=arm

echo "----------------------------------"
echo "setting up Release-arm environment"
echo "----------------------------------"

gn gen out/Release-arm
echo 'target_cpu = "arm"' >> out/Release-arm/args.gn
gn gen out/Release-arm

echo "----------------------------------"
echo "silent first build (needs to fail)"
echo "----------------------------------"

ninja -C out/Release-arm quic_server quic_client > /dev/null
cd out
patch -p1 < ../../../quic_client-arm.ninja.patch

echo "----------------------------------"
echo "           final build"
echo "----------------------------------"

cd ..
ninja -C out/Release-arm quic_server quic_client
